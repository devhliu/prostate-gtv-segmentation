# Prostate GTV segmentation
This repository contains the source code used for the work in paper ['Intraprostatic Tumour Segmentation on PSMA-PET Images in Patients with Primary Prostate Cancer with a Convolutional Neural Network'](http://jnm.snmjournals.org/content/early/2020/11/06/jnumed.120.254623). It was tested on Ubuntu 18.04 LTS.

## Prerequisites
You need to have installed:
- ```Python 3.7.3``` [(https://www.python.org/)](https://www.python.org/) or higher
- ```CUDA Version 10.1``` or higher (optional for better performance) [(download link)](https://developer.nvidia.com/cuda-downloads)
- For visualization of the PSMA PET data I used ```3d slicer 4.10.1``` [(https://www.slicer.org/)](https://www.slicer.org/).

## Folder structure
- [data](data/): contains the data (see section [Add the data](#add-the-data)) 
- [src](src/): contains all the source code.
- [results](results/): folder where results can be stored. Is created automatically, if no results folder option is committed.

## How to use
### Download the source code
```git clone https://gitlab.com/dejankostyszyn/prostate-gtv-segmentation.git```

```cd prostate-gtv-segmentation/```

### Installing requirements  
I recommend using a virtual environment (optional):
```
sudo apt update
sudo apt install virtualenv # Install virtual environment.
virtualenv venv -p python3  # Create virtual environment with python 3.
source venv/bin/activate    # Activate virtual environment.
```

Upgrade pip and install requirements:
```
pip3 install -U pip
pip3 install -r requirements.txt
```
If the Pytorch library does not match your CUDA version, see [here](https://pytorch.org/get-started/locally/).

### Add the data
Copy the data you want to use for the computations into the folder [prostate-gtv-segmentation/data](prostate-gtv-segmentation/data). The folder structure **must** be as follows:
```
data/ [custom_dataset]--> [data_description].csv
                      --> [patient_folder1]/--> [pet_file].nrrd
                                            --> [prostate_kontur_file].nrrd
                                            --> [lesion1].nrrd
                                            --> [lesion2].nrrd
                                            ...
                      --> [patient_folder2]/--> [pet_file].nrrd
                                            --> [prostate_kontur_file].nrrd
                                            --> [lesion1].nrrd
                                            --> [lesion2].nrrd
                                            ...
                      ...
```

A CSV file describes where the patients' data is stored. The CSV file must contain the folder names and file names in the following format:

```
patient_folder,PET_file,prostate_contour,cancer_lesion,Gleason_Score
```

For example:
```
patient_folder,PET_file,prostate_contour,cancer_lesion,Gleason_Score
patient_folder0,patient0_pet.nrrd,patient0_prostate.nrrd,patient0_GTV.nrrd,2
patient_folder1,p1_pet.nrrd,prostateP1.nrrd,patient1-GTV.nrrd,0
```
If no GS is available type ```2``` or something else than ```0``` or ```1```.


### Verify data paths
First one should verify that the program uses correct paths to the data.

```python3 src/print_data_paths.py --data_root data/[custom_dataset]/ --csv_file data/[data_description].csv --results_path results/data_paths/```

### Verify correct reading of the data
This will read all the available data one times. I recommend enabling the ```--check_consistency``` flag. This will additionaly read all NRRD headders and compare them to check consistency. If the headders of a patient don't match, the program will interrupt. Additionally it will check that all tensors contain at least one element that is not zero. However, if you need empty tensors, don't use the ```--check_consistency``` flag.

```python3 src/test_data_reading.py --data_root data/[custom_dataset]/ --csv_file data/[data_description].csv --check_consistency```

### Compute overall standard deviation and arithmetic mean for normalization
1. If you want to use a manual training permutation open file [src/get_normalization_params.py](src/get_normalization_params.py) and change the ```idx_train``` list to your training permutation. Otherwise use the ```--shuffle``` flag to use a random permuation. Use this flag also for training and testing procedure and you will be using the same permutation for training and testing.
2. ```python3 src/get_normalization_params.py --data_root data/[custom_dataset]/ --csv_file data/[data_description].csv --shuffle```

3. Fill in the values computed for your dataset into [src/ReadData.py](src/ReadData.py)'s ```__init__()``` method, e.g.:
```python
self.std = 4.060953698510707 # Standard deviation to manually change.
self.mean = 1.3583295982641057 # Arithmetic mean to manually change.
```

### Train and validate segmentation network
```
python3 src/train.py --data_root data/[custom_dataset]/ --csv_file data/[data_description].csv --results_path results/my_train_results/  --loss_fn cross_entropy --augmentation none --shuffle -store_loaded_data
```

To have a look at the available training options run ```python3 src/train.py -h```.

### Test segmentation network
Our trained model is provided in this git repo at "results/my_train_results/triained_model.sausage". **ATTENTION!** For testing and prediction of single GTVs the test and predict procedure need to use the same settings as the model was trained with. Otherwise the **results** will most likely be **very poor**! These settings were ```--normalize global``` and the corresponding normalization parameters as described above:
```python
self.std = 4.060953698510707
self.mean = 1.3583295982641057
```

```
python3 src/test.py --data_root data/[custom_dataset]/ --csv_file data/[data_description].csv --results_path results/my_test_results --trained_model results/my_train_results/trained_model.sausage --shuffle
```

To have a look at the available test options run ```python3 src/test.py -h```.

### Predict GTV from PET/PSMA file and Prostate Contour
If you would like to predict the GTV of a single patient's PET and prostate contour run
```
python3 src/predict.py --pet_file data/[custom_dataset]/[patient_folder0]/[patient0_pet].nrrd --prostate_contour data/[custom_dataset]/[patient_folder0]/[patient0_prostate].nrrd --trained_model results/my_train_results/trained_model.sausage --prediction_name results/prediction_patient0.nrrd
```
**ATTENTION!** Use the same settings for prediction as were used for training the trained model. In case you use our trained model, use ```--normalize global```. If you use your own trained model that was trained with different normalization parameters change std and mean in [src/predict.py](src/predict.py).
