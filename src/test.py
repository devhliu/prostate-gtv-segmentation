"""
Copyright Dejan Kostyszyn 2019

This method trains the network.
"""

import ReadData as rd
import numpy as np
from matplotlib import colors
import torch, os, nrrd, time
from model.UNet import UNet
import matplotlib.pyplot as plt
from options.test_options import TestOptions
import utils, csv, sys
from os.path import join

def test():
    print("initializing...")
    start_time = time.time()

    # Initializing.
    opt = TestOptions().parse()
    vl = rd.ValLoader(opt)

    max_idx = vl.nr_of_patients()
    results_path = join(opt.results_path)
    trained_model_name = opt.trained_model

    # Check if results folder exists and if so, ask user if really want to continue.
    utils.overwrite_request(results_path)

    # Create folders and files.
    utils.create_folder(results_path)
    with open(results_path + "/test_results.csv", "w", newline="") as file:
            writer = csv.writer(file, delimiter=",", quotechar='|', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(["img idx", "mean cross entropy loss", "mean DSC", "median DSC",\
                "mean HD", "median HD", "mean average surface distance", "median average surface distance",\
                "mean average symmetric surface distance", "mean precison", "mean sensitivity", "median sensitivity",\
                "mean specificity", "median specificity", "std DSC", "std HD", "std ASD", "std ASSD", "std precision", "std sensi", "std speci",
                "median time", "min time", "max time", "median time forw.", "min time forw.", "max time forw."])
    with open(results_path + "/test_results_detailed.csv", "w", newline="") as file:
            writer = csv.writer(file, delimiter=",", quotechar='|', quoting=csv.QUOTE_MINIMAL)
            writer.writerow(["img idx", "cross entropy loss", "dice coefficient",\
                "hausdorff distance", "average surface distance",\
                "average symmetric surface distance", "precison", "sensitivity",\
                "specificity", "patient id", "computation time complete", "computation time forw. pass"])

    # Write options into a file.
    with open(results_path + '/test_options.txt', 'w') as f:
        f.write(" ".join(sys.argv[1:]))

    """
    For direct GS prediction 3 classes:
    0: lesion with gs = 7, 1: lesion with gs > 7, 2: no lesion.

    For segmentation without GS prediction 2 classes:
    0: no lesion, 1: lesion.
    """
    
    num_classes = 2

    overall_test_loss = []
    detailed_test_loss = [[],[],[],[],[],[],[],[]]
    
    # Divide data into training, validation and test set.
    idx_train, idx_val, idx_test = utils.divide_data(max_idx, opt)

    # Insert manual training, validation and test permutation here if you wish.
    # idx_train = [72,60,132,71,31,101,144,158,96,166,164,161,125,56,124,93,121,70,45,57,122,29,77,49,90,82,48,159,66,128,38,150,58,171,44,163,120,22,21,30,137,33,52,138,140,102,80,106,94,110,105,34,97,88,160,41,165,154,111,113,67,89,61,115,112,26,63,119,149,155,157,73,51,130,116,174,156,133,143,98,95,39,131,50,126,100,85,59,43,167,135,134,141,142,74,91,87,68,145,78,170,108,25,62,103,153,69,76,40,139,129,117,81,86,84,65,118,92,55,37,99,83,32,0,169,79,19,148,136,35,36,104,23,168,42,173,123,127,162,109,172,54,107,147,146]
    # idx_val = [53,27,75,151,152,2,24,114,64,47,28,46]
    # idx_test = [1, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 20]

    if not opt.no_data_info == True:
        print("# train images = {}, # val images = {}, # test images = {}".format(len(idx_train), len(idx_val), len(idx_test)), end="\n\n")
        print("train images = \t{}".format(idx_train))
        print("val images = \t{}".format(idx_val))
        print("test images = \t{}".format(idx_test))

    data, lesion, _, _, _, _ = vl.val_loader(is_train=False)
    
    model = UNet(data.shape, lesion.shape, num_classes, opt)
    # Load pretrained model.
    model.load_state_dict(torch.load(trained_model_name))
    
    # Enabling GPU
    device = torch.device('cuda:0' if torch.cuda.is_available() else 'cpu')
    model = model.to(device)

    # Set loss function.
    loss_fn = utils.set_loss_fn(opt, num_classes, device)

    complete_computation_times = []
    forward_computation_times = []
    
    # Perform testing.
    model.eval()
    print("started testing...")
    with torch.no_grad():
        for idx in idx_test:
            global_start = time.time()
            data, lesion, p_id, pet_h, ratio, old_pet_shape = vl.val_loader(idx=idx, is_train=False)
            data = data.to(device)
            lesion = lesion.to(device)

            # Predict.
            fw_pass_start = time.time()
            y_pred = model(data.unsqueeze(0))
            fw_pass_end = time.time()

            # Computing and saving losses.
            # Computing loss.
            if opt.loss_fn == "dice":
                loss = loss_fn(y_pred.squeeze(), lesion.float())
            else:
                loss = loss_fn(y_pred, lesion.long().unsqueeze(0))
            detailed_test_loss[0].append(loss.item())
            prediction = torch.argmax(y_pred.squeeze(0), dim=0).float()
            prediction = prediction.cpu().detach().numpy()
            detailed_test_loss = utils.compute_losses(prediction, lesion.cpu().detach().numpy(), detailed_test_loss)

            print("img idx: {}, p_id: {}, cross entropy loss: {}, dice coeff.: {}".format(idx, p_id, detailed_test_loss[0][-1], detailed_test_loss[1][-1]), end="")

            # Write prediction into file.
            minx, maxx, miny, maxy, minz, maxz = old_pet_shape[1:]
            prediction = utils.fit_prediction_to_pet(old_pet_shape[0], prediction, minx, maxx, miny, maxy, minz, maxz, opt)
            filename = results_path + "/" + p_id + "_prediction.nrrd"
            nrrd.write(filename, prediction, pet_h)

            global_end = time.time()

            with open(results_path + "/test_results_detailed.csv", "a", newline="") as file:
                writer = csv.writer(file, delimiter=",", quotechar='|', quoting=csv.QUOTE_MINIMAL)
                writer.writerow([idx, detailed_test_loss[0][-1], detailed_test_loss[1][-1], detailed_test_loss[2][-1],\
                    detailed_test_loss[3][-1], detailed_test_loss[4][-1], detailed_test_loss[5][-1], detailed_test_loss[6][-1], detailed_test_loss[7][-1], p_id,\
                    str(global_end - global_start), str(fw_pass_end - fw_pass_start)])

            forward_computation_times.append(fw_pass_end - fw_pass_start)
            complete_computation_times.append(global_end - global_start)

            print(", time: {}s, time forw. pass: {}s".format(global_end - global_start, fw_pass_end - fw_pass_start))
    
    for i in range(np.asarray(detailed_test_loss).shape[0]):
        overall_test_loss.append(np.mean(detailed_test_loss[i]))

    # Plotting test losses.
    utils.plot_losses(opt, results_path, "", "image",\
        "loss", "dice_coefficient_test.png",\
        [detailed_test_loss[1], "test (dice coeff)"],\
        axis=[-1, len(idx_test), 0, 1.1])
    utils.plot_losses(opt, results_path, "", "image",\
        "loss", "hausdorff_distance_test.png", \
        [detailed_test_loss[2], "test (hd)"],\
        [detailed_test_loss[3], "test (asd)"],\
        [detailed_test_loss[4], "test (assd)"])
    utils.plot_losses(opt, results_path, "", "image",\
        "loss", "other_metrics_test.png",\
        [detailed_test_loss[5], "test (precision)"],\
        [detailed_test_loss[6], "test (sensitivity)"],\
        [detailed_test_loss[7], "test (specificity)"],\
        axis=[-1, len(idx_test), 0, 1.1])

    # Writing overall test loss in file.
    with open(results_path + "/test_results.csv", "a", newline="") as file:
        writer = csv.writer(file, delimiter=",", quotechar='|', quoting=csv.QUOTE_MINIMAL)
        writer.writerow([idx, np.mean(detailed_test_loss[0]), np.mean(detailed_test_loss[1]), np.median(detailed_test_loss[1]), np.mean(detailed_test_loss[2]), np.median(detailed_test_loss[2]),\
            np.mean(detailed_test_loss[3]), np.median(detailed_test_loss[3]), np.mean(detailed_test_loss[4]), np.mean(detailed_test_loss[5]), np.mean(detailed_test_loss[6]), np.median(detailed_test_loss[6]),\
            np.mean(detailed_test_loss[7]), np.median(detailed_test_loss[7]), np.std(detailed_test_loss[1]), np.std(detailed_test_loss[2]), np.std(detailed_test_loss[3]), np.std(detailed_test_loss[4]),\
            np.std(detailed_test_loss[5]), np.std(detailed_test_loss[6]), np.std(detailed_test_loss[7]), np.median(complete_computation_times), np.min(complete_computation_times),
            np.max(complete_computation_times), np.median(forward_computation_times), np.min(forward_computation_times), np.max(forward_computation_times)])


    end_time = time.time()
    print("Completed testing in {0} with\n overall cross entropy loss: {1}, dice coefficient: {2}".format(end_time - start_time, overall_test_loss[0], overall_test_loss[1]))

if __name__ == "__main__":
    test()